# Upsource template for BastilleBSD

Template for [BastilleBSD](https://bastillebsd.org/) to run a
[Upsource](https://www.jetbrains.com/upsource/) service inside of a
[FreeBSD](https://www.freebsd.org/) jail.

By default the hard coded version of Upsource (see
[Bastillefile](Bastillefile)) will be installed using default settings.

Specify an external mounted directory for backups which will be mounted to
`${BACKUP_DIR}` and configure your installation accordingly.

**Please note that you have to enable Linux support for it to run correctly!**

**Due to issues with the used database this has to be a _thick_ jail!**

## License

This program is distributed under 3-Clause BSD license. See the file
[LICENSE](LICENSE) for details.

## Bootstrap

So far bastille only supports downloading from GitHub or GitLab, so you have
to fetch the template manually:

```
# mkdir <your-bastille-template-dir>/wegtam
# git -C <your-bastille-template-dir>/wegtam clone https://codeberg.org/wegtam/bastille-upsource.git
```

## Usage

### 1. Install the default version into a jail

```
# bastille template TARGET wegtam/bastille-upsource --arg EXT_DIR=/srv/backups/upsource
```

### 2. Install with custom settings

```
# bastille template TARGET wegtam/bastille-upsource --arg EXT_DIR=/srv/backups/upsource --arg VERSION=2020.1.1883
```

For more options take a look at the [Bastillefile](Bastillefile).

